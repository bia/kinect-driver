/**
 * 
 */
package plugins.fab.kinectdriver;

import icy.plugin.abstract_.Plugin;

/**
 * Kinect driver class used just to use this plugin as simple Kinect Driver
 * 
 * @author Stephane Dallongeville
 */
public class KinectDriverPlugin extends Plugin
{
    public KinectDriverPlugin()
    {
        super();
        
        // nothing to do here,  we just just this class for Kinect native library loading
    }
}
