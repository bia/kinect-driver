package plugins.fab.kinectdriver;

public class KinectData {

	public float[] xyz;
	public float[] uv;
	
	public KinectData(float[] xyz, float[] uv) {
		this.xyz = xyz;
		this.uv = uv;
	}

}
